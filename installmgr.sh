#!/bin/bash

set -e

case $(uname -m) in
x86_64)
    ARCH=amd64;;
aarch64)
    ARCH=arm64;;
*)
    echo Unknown arch $(uname -m)
    exit 1
    ;;
esac

if test ! -f /etc/systemd/system/ivxmgr.service; then
    cat >/etc/systemd/system/ivxmgr.service << _END_
[Unit]
Description=Ivxmgr service
After=network.target

[Service]
Type=simple
User=root
Group=root

Restart=always
RestartSec=20

WorkingDirectory=/opt/ivxbase
ExecStart=/opt/ivxbase/ivxmgr

[Install]
WantedBy=multi-user.target
_END_
else
    systemctl stop ivxmgr
fi

mkdir -p /opt/ivxbase

URL=https://file3.ih5.cn/ivxmgr_$ARCH
OUT=/opt/ivxbase/ivxmgr

if ! wget -q $URL -O $OUT; then
	if ! curl -fsSL $URL -o $OUT; then
		echo must have wget or curl
		exit 1
	fi
fi

chmod +x $OUT

if test ! -f $OUT.json; then
    cat >$OUT.json << _END_
{"http": ${PORT:=80}, "local": ${LOCAL:=8000}}
_END_
fi

if test ! -f /etc/systemd/system/ivxbase.service; then
    cat >/etc/systemd/system/ivxbase.service << _END_
[Unit]
Description=Ivxbase service
After=network.target

[Service]
Type=simple
User=root
Group=root

Restart=always
RestartSec=20

WorkingDirectory=/opt/ivxbase
ExecStart=/opt/ivxbase/ivxbase

[Install]
WantedBy=multi-user.target
_END_
fi

systemctl daemon-reload
systemctl enable ivxmgr
systemctl start ivxmgr
